# Supporting VNF chains: an implementation using Segment Routing and PCEP
This repository contains documentation as well as tools and configuration for the research.

# The network
![High detail network](https://bitbucket.org/uva-sne/ron20_sr/raw/395a900ae09c3ed2857052d6d4de680fb61c70bd/imgs/network-high-detail.svg)  
A high detail network view is shown above. The vqfx interface ids are shortened, i.e xe-0/0/1 is shown as xe1.
Not shown in this image is that each VM has a seperate managment interface, which can be accessed via Vagrant.
For example, to connect to vqfx2, use `vagrant ssh vqfx2`

## Configurations
The Junos vqfx configurations as well as the FRR configs are shown in the configurations (`configs`)

## Network setup
The network is spun up using Vagrant on a libvirt backend.
See the `Vagrantfile` for more details on the exact connections made.

Importantly, the VQFX and Ubuntu boxes should be loaded in Vagrant as these are not available on the Vagrant Cloud.
```
vagrant box add ubuntu18.04_bpf_next.libvirt.box --name ubuntu18.04_bpf_next
vagrant box add vqfx-19.4R1.10-re.libvirt.box --name vqfx-19.4R1.10-re
vagrant box add vqfx-19.4R1.10-pfe.libvirt.box --name vqfx-19.4R1.10-pfe
```

After the vagrant network is brought up, the configurations are manually loaded.
This is not done inside of Vagrant because of the potential for timing issues with Junos.
Additionally, using an external script makes it easy to replace configurations.
The configurations are loaded via the `/scripts/load_junos_cfg.py` via `load-configs.sh`

# The coordination and BPF tools
Python version 3.7  (3.6, the default on Ubuntu will not work)
The tool used to coordinate the VNFs with the SR-LSPs is the Python module `sr-vnf`.  

Most useful commands can be shown via the command `python3.7 -m sr-vnf --help`  
The tool requires a configuration file to be present, tuned to the specific environment.
See `sr-vnf/config.example.yaml`, for an example for the network shown before.


## Examples
Listing the SR-LSPs  
```python3.7 -m sr-vnf --controller northstar list```

Adding a sr-lsp  
```python3.7 -m sr-vnf --controller northstar lsp add sr-path vqfx1 host12 vqfx4```
Removing that sr-lsp:  
`python3 -m sr-vnf --controller ns lsp remove sr-path`

Migrating MPLS_DUMMY from host12 to host13
`python3 -m sr-vnf --controller ns migrate sr-path host12 host13 MPLS_DUMMY`

Chaining host13 after host12, with MPLS_FW on host13:  
`python3 -m sr-vnf --controller ns chain sr-path host12 host13 MPLS_FW`

## VNF BPF wrapper
The VNF interactions are taken care of by the tool in the `nfv-py` folder.  
This tool can be used fully standalone as a python module: `python3.7 -m nfv-py --help`  
It runs locally on a machine running the BPF backend.
This is available from repository: (https://bitbucket.org/uva-sne/nfv/)[https://bitbucket.org/uva-sne/nfv/]

Example list vnfs on host12
```
root@host12:~# python3 -m nfv-py --list
Loaded & attached programs
name: id
SR_PROXY: 64
MIRROR: 63
MPLS_DUMMY: 65
NFV_SWITCH: 61
```

# Notes & Debugging
For most of the common issues and problems with the environment, see `COMMON-ISSUES.md`

# Acknowledgment
Part of this work has been supported by SURFnet under the RoN - Research on Network program - 2020
This work was made possible thanks to the prior work and support from Łukasz Makowski. Juniper supported this research by providing the NorthStar Controller software.