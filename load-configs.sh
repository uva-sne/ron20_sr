#!/bin/bash
"""
Basic wrapper script to load all the configurations
"""
python3 ./scripts/load_junos_cfg.py vqfx1 ./configs/vqfx1.cfg
python3 ./scripts/load_junos_cfg.py vqfx2 ./configs/vqfx2.cfg
python3 ./scripts/load_junos_cfg.py vqfx3 ./configs/vqfx3.cfg
python3 ./scripts/load_junos_cfg.py vqfx4 ./configs/vqfx4.cfg