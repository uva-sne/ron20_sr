# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 2.1.0"
VAGRANTFILE_API_VERSION = "2"

## Generate a unique ID for this project
UUID = "OTYUID"
def get_mac(oui="08:00:27")
    "Generate a MAC address"
    nic = (1..3).map{"%0.2x"%rand(256)}.join(":")
    return "#{oui}:#{nic}"
  end
## Define ports mapping to create a Full Mesh between all 4 vqfx
core_map = { 1 => [12,13,14],
              2 => [12,23,24],
              3 => [13,23,34],
              4 => [14,24,34],
              11 => [11],
              12 => [12],
              13 => [13],
              14 => [14]}
core_reverse_map = {
    12 => {}
}

host_map = {
    1 => [11],
    2 => [12],
    3 => [13],
    4 => [14],
}

host_reverse_map = {
    11 => [11],
    12 => [12],
    13 => [13],
    14 => [14]
}

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    config.ssh.insert_key = false

    # config.trigger.after [:up] do |trigger|
    #   trigger.info = "Running ./scripts/vagrant_ssh.sh locally..."
    #   trigger.run = {path: "./scripts/vagrant_ssh.sh"}
    # end

    # config.trigger.after [:up] do |trigger|
    #     trigger.info = "Running ./scripts/vagrant_ssh.sh locally..."
    #     trigger.run = {path: "./scripts/vagrant-ssh-config.sh"}
    # end


    (1..4).each do |id|
        re_name  = ( "vqfx" + id.to_s ).to_sym
        pfe_name = ( "vqfx" + id.to_s + "-pfe" ).to_sym

        # ##############################
        # ## Packet Forwarding Engine ##
        # ##############################
        config.vm.define pfe_name do |vqfxpfe|
            vqfxpfe.ssh.insert_key = false
            vqfxpfe.vm.box = 'vqfx-10k-pfe'

            vqfxpfe.vm.boot_timeout = 1200
            vqfxpfe.vm.guest = :tinycore

            vqfxpfe.vm.synced_folder '.', '/vagrant', disabled: true
           
            vqfxpfe.vm.provider :libvirt do |domain|
                domain.cpus = 1
                domain.memory = 2048
                domain.disk_bus = "ide"
                domain.nic_adapter_count = 1
                domain.nic_model_type = "e1000"
            end
            
            # vqfx-pfe-int1 <--> vqfx-re-int1
            vqfxpfe.vm.network :private_network,
              :mac => "#{get_mac()}",
              :libvirt__tunnel_type => "udp",
              :libvirt__tunnel_local_ip => "127.15.121."+id.to_s+"2",
              :libvirt__tunnel_local_port => 10001,
              :libvirt__tunnel_ip => "127.15.121."+id.to_s+"1",
              :libvirt__tunnel_port => 10001,
              :libvirt__iface_name => "internal",
              auto_config: false
            
            
        end

        ##########################
        ## Routing Engine  #######
        ##########################
        config.vm.define re_name do |vqfx|
            vqfx.vm.hostname = "vqfx#{id}"
            #vqfx.vm.box = 'juniper/vqfx10k-re'
            vqfx.vm.box = "vqfx-19.4R1.10-re"
            vqfx.vm.guest = :tinycore
            vqfx.vm.boot_timeout = 1200

            vqfx.vm.synced_folder '.', '/vagrant', disabled: true

            vqfx.vm.provider :libvirt do |domain|
                domain.cpus = 1
                domain.memory = 1024
                domain.disk_bus = "ide"
                domain.nic_adapter_count = 12
                domain.nic_model_type = "e1000"
            end

            # vqfx-re-01-int1 <--> vqfx-pfe-01-int1
            vqfx.vm.network :private_network,
                :mac => "#{get_mac()}",
                :libvirt__tunnel_type => "udp",
                :libvirt__tunnel_local_ip => "127.15.121."+id.to_s+"1",
                :libvirt__tunnel_local_port => 10001,
                :libvirt__tunnel_ip => "127.15.121."+id.to_s+"2",
                :libvirt__tunnel_port => 10001,
                :libvirt__iface_name => "internal",
                auto_config: false
            
            
            # vqfx-re-int2 reserved interface
            vqfx.vm.network :private_network,
                :mac => "#{get_mac()}",
                :libvirt__tunnel_type => "udp",
                :libvirt__tunnel_local_ip => "127.15.121."+id.to_s+"1",
                :libvirt__tunnel_local_port => 10002,
                :libvirt__tunnel_ip => "127.6.6."+id.to_s+"6",
                :libvirt__tunnel_port => 10666,
                :libvirt__iface_name => "bh-int" + id.to_s,
                auto_config: false

            # Dataplane core ports
            # attached via tunnels to avoid the annoying bridge effect from normal
            # private networks
            # ip: "10.0.#{ports_map[id][seg_id]}.10#{id}",
            core_map[id].each do |seg_id|
                vqfx.vm.network :private_network,
                    :libvirt__tunnel_type => "udp",
                    :libvirt__tunnel_local_ip => "127.16.#{seg_id}.#{id}",
                    :libvirt__tunnel_local_port => 10001,
                    :libvirt__tunnel_ip => "127.16.#{seg_id}.#{+ seg_id.to_s.sub(id.to_s, "")}",
                    :libvirt__tunnel_port => 10001,
                    :libvirt__iface_name => "core#{seg_id}",
                    auto_config: false
                    # libvirt__network_name: "core#{seg_id}",
                    # ip: "10.0.#{seg_id}.#{id}"
            end

            # The host connections
            host_map[id].each do |seg_id|
                vqfx.vm.network :private_network,
                    :libvirt__tunnel_type => "udp",
                    :libvirt__tunnel_local_ip => "127.17.#{seg_id}.1",
                    :libvirt__tunnel_local_port => 10001,
                    :libvirt__tunnel_ip => "127.17.#{seg_id}.2",
                    :libvirt__tunnel_port => 10001,
                    :libvirt__iface_name => "host#{seg_id}",
                    auto_config: false

                    # libvirt__network_name: "host#{seg_id}",
                    # ip: "192.168.#{seg_id}.1"
            end
            # Define network to Northstar
            if (id == 3)
                vqfx.vm.network "private_network",
                    libvirt__network_name: "northstar",
                    ip: "10.0.36.#{id}"
            end
            # Define network to opendaylight
            if (id == 3)
                vqfx.vm.network "private_network",
                    libvirt__network_name: "opendaylight",
                    ip: "10.0.37.#{id}"
            end

            # Load the configuration
            # vqfx.trigger.after [:up, :provision] do |trigger|
            #     trigger.run = {path: "./scripts/load_junos_cfg.py", args: "#{vqfx.vm.hostname} configs/#{vqfx.vm.hostname}.cfg"}
            #  end
        end
    end

    ##########################
    ## Test hosts      #######
    ##########################
    (11..14).each do |id|
        host_name  = ( "host" + id.to_s ).to_sym

        config.vm.define host_name do |host|
            host.vm.hostname = "host#{id}"
            host.vm.box = "ubuntu18.04_bpf_next"
            # host.vm.provider = "libvirt"

            host.vm.synced_folder '.', '/vagrant', disabled: true
            # host.vm.synced_folder './scripts', '/vagrant/scripts', type: "rsync"
            # host.vm.synced_folder './configs', '/vagrant/configs', type: "rsync"
            # host.vm.synced_folder './kernel-comp/debs', '/vagrant/debs/', type: "rsync"

            host.vm.boot_timeout = 600

            host.vm.provider :libvirt do |domain|
                domain.cpus = 1
                domain.memory = 1024
            end

            host_reverse_map[id].each do |seg_id|
                host.vm.network :private_network,
                    :libvirt__tunnel_type => "udp",
                    :libvirt__tunnel_local_ip => "127.17.#{seg_id}.2",
                    :libvirt__tunnel_local_port => 10001,
                    :libvirt__tunnel_ip => "127.17.#{seg_id}.1",
                    :libvirt__tunnel_port => 10001,
                    :libvirt__iface_name => "host#{seg_id}",
                    auto_config: false
                    # libvirt__network_name: "host#{seg_id}",
                    # ip: "192.168.#{seg_id}.2"
            end
        end
    end

    # Northstar host
    host_name  = ( "northstar" ).to_sym
    config.vm.define host_name do |host|
        host.vm.hostname = "northstar"
        host.vm.box = 'centos/7'
        host.vm.synced_folder '.', '/vagrant', disabled: true
        host.vm.boot_timeout = 600

        host.vm.provider :libvirt do |domain|
            domain.cpus = 8
            domain.memory = 16384
            domain.nested = true
            domain.nic_adapter_count = 2
            domain.machine_virtual_size = 30
        end
        # Forward the port needed for the NorthStar GUI
        host.vm.network "forwarded_port", guest: 8443, host: 60443

        host.vm.network "private_network",
            libvirt__network_name: "northstar",
            ip: "10.0.36.10"
    end

end
