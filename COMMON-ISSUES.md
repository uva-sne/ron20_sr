# Vagrant/VQFX Issues

## Vagrant up times out on vqfx's.
Likely, the vqfx's booted up in linecard mode, instead of master mode.
Thus, their managment interface doesn't correctly come up

1. Console into the vqfx `virsh console nfv_frr_console`
2. Em0 probably is coming up as `eth-switch`
3. Likely when logging in you get a linecard warning
4. Solution, reboot the vqfx: `request system reboot`

# NorthStar Issues

## NorthStar shows no connections
The NorthStar GUI doesn't show any connections.
This is due to the NorthStar Junos VM not retaining configurations after reboots.
The configuration stored in `configs/northstar.cfg` will have to be restored to the VM
By default, there is a stored config present on the norhstar vm, in `set` style, for easier loading (`junosvm-set.conf`).
Otherwise, the config can be manually loaded