from subprocess import run
import subprocess
import json
import argparse
"""
Wrapper code for the SR-Aware MPLS VNFs control
Uses the BPF tool functionality
"""

PROG_IDS = {
    "SR_PROXY_FUNC_ID": 0,
    "MPLS_FW_FUNC_ID": 1,
    "MIRROR_FUNC_ID": 4,
    "NFV_SWITCH_FUNC_ID": 7,
    "MPLS_DUMMY_FUNC_ID": 6,
}

def id_to_name(id):
    for name, key_id in PROG_IDS.items():
        if key_id == id:
            return name

def get_prog_map():
    """
    Find the correct map ID for the FUNC_MAP
    """
    MAP_PIN =  "/sys/fs/bpf/tc/globals/FUNC_SWITCH_MAP"

    res = run([
        "bpftool",
        "map",
        "show",
        "-f",
        "-p"
    ], stdout=subprocess.PIPE)
    maps = json.loads(res.stdout)

    for m in maps:
        if 'pinned' not in m:
            continue

        if MAP_PIN in m['pinned']:
            return m['id']


def dettach_prog(func_map_id, func_key):
    """
    Dettaches the program by removing it's entry in the func map
    """

    # bpftool map pinned /sys/fs/bpf/tc/globals/FUNC_SWITCH_MAP delete key function_key 00 00 00
    run([
        "bpftool",
        "map",
        "delete",
        "id",
        str(func_map_id),
        "key",
        str(func_key),
        "00",
        "00",
        "00"
    ])


def get_prog_ids():
    """
    Get the ids for the loaded programs
    """
    res = run([
        "bpftool",
        "net",
        "show",
        "-p"
    ], stdout=subprocess.PIPE)

    entries = json.loads(res.stdout)

    # Just get the tc filters as we do not use xdp
    entries = entries[0]['tc']
    prog_ids = {}
    for e in entries:
        func_name = e['name'].split('.bpf')[0].upper()
        loaded_id = e['id']
        prog_ids[func_name.upper()] = loaded_id
    
    return prog_ids


def hex_to_int(hexlist):
    intstr = ""
    
    hexlist = reversed([x.replace("0x", "") for x in hexlist])

    hexstr = "0x"+ "".join(hexlist)

    return int(hexstr, 16)


def list_programs(func_map_id):
    """
    Return the programs attached to NFV Switch
    via the prog map
    """
    res = run([
        "bpftool",
        "map",
        "dump",
        "id",
        str(func_map_id),
        "-p"
    ], stdout=subprocess.PIPE)

    entries = json.loads(res.stdout)

    func_ids = {}
    for entry in entries:
        value = entry["value"]
        func_id_key = entry["key"]
        func_prog_key = hex_to_int(value)
        func_id = hex_to_int(func_id_key)
        func = id_to_name(func_id).replace("_FUNC_ID", "")

        func_ids[func] = func_prog_key
    
    return func_ids

def attach(func_map_id, prog_key, loaded_prog_id):
    """
    Insert the given prog id into the func map
    """

    # bpftool map update pinned /sys/fs/bpf/tc/globals/FUNC_SWITCH_MAP key 7 0 0 0 value id 303

    command = ["bpftool",
        "map",
        "update",
        "pinned",
        "/sys/fs/bpf/tc/globals/FUNC_SWITCH_MAP",
        "key",
        str(prog_key),
        "0",
        "0",
        "0",
        "value",
        "id",
        str(loaded_prog_id)
    ]
    run(command)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    program_choices = [x.replace("_FUNC_ID", "") for x in PROG_IDS.keys()]
    parser.add_argument("--detach", 
         choices=program_choices,
        help=\
        """
        Program to detach from the nfv switch
        """)

    parser.add_argument("--attach", 
         choices=program_choices,
        help=\
        """
        Program to attach to the nfv switch
        """)
    
    parser.add_argument("--list", action="store_true",
        help="list the loaded & attached programs, with ID")

    parser.add_argument("--json", "-j", action="store_true",
        help="Format the json in data before printing")


    args = parser.parse_args()

    do_json = args.json

    func_map_id = get_prog_map()
    if func_map_id == None:
        print("Can't fine the func switch map")
        exit(-1)

    if args.detach:

        print("Unloading {}".format(args.detach))

        func_id = PROG_IDS["{}_FUNC_ID".format(args.detach)]

        # Check if it is actually loaded
        if args.detach not in list_programs(func_map_id).keys():
            print("{} not attached".format(args.detach))
            exit(-1)

        dettach_prog(func_map_id, func_id)

    elif args.list == True:
        func_ids = list_programs(func_map_id)
        if not do_json:
            print("Loaded & attached programs")
            print("name: id")
            for func, key in func_ids.items():
                print("{}: {}".format(func, key))
        else:

            json_str = json.dumps(func_ids)
            print(json_str)


    elif args.attach:
        print("Attaching {}".format(args.attach))
        
        progname = args.attach
        # check if it isn't already loaded
        if progname in list_programs(func_map_id).keys():
            print("Already attached")
            exit(0)
        
        # Get the prod_id
        prog_ids = get_prog_ids()
        # get the correct one
        loaded_id = prog_ids[progname]
        print("Attaching via {}".format(loaded_id))
        prog_key = PROG_IDS[progname + "_FUNC_ID"]
        print(prog_key)
        attach(func_map_id, prog_key, loaded_id)