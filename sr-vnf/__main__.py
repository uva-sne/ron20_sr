from typing import Type, List, Dict
from subprocess import run
import subprocess
import json
import yaml
import argparse
import time

from .sdn import SDN
from .odl import OpenDayLight
from .northstar import NorthStar
from .vnf import get_vnfs, deactivate_vnf, activate_vnf, PROG_IDS


def migrate(sdn: Type[SDN] , lsp, h1, h2, vnf, config):
    print("Migrating {} from {} to {}".format(vnf, h1, h2))
    hosts = config["hosts"]
    host1 = [x for x in hosts if x["name"] == h1][0]
    host2 = [x for x in hosts if x["name"] == h2][0]
    # Step 1: activate the VNF on host2
    print("Activate VNF {} on {}".format(vnf, h2))
    activate_vnf(host2["name"], vnf)

    # Step 2: change the LSP
    print("=================")
    print("Changing the LSP")
    start_time = subprocess.run(["date", "+%s%N"])
    print("Start time stamp (ns): {}".format(start_time))
    sdn.migrate_lsp(lsp, h1, h2)
    end_time = subprocess.run(["date", "+%s%N"])
    print("End time stamp (ns): {}".format(end_time))

    # Step 3: disable the VNF on host1
    print("=================")
    print("Deactivate VNF {} on {}".format(vnf, h1))
    deactivate_vnf(host1["name"], vnf)

def chain(sdn : Type[SDN], lsp, previous_h, h, vnf, config):
    """
    Chains a new VNF in the given LSP
    Starts the VNF on the given host, and adds the host
    on the path after the previous_h
    """

    hosts = config["hosts"]
    prev_h = [x for x in hosts if x["name"] == previous_h][0]
    host = [x for x in hosts if x["name"] == h][0]

    print("Activate VNF {} on {}".format(vnf, host))
    activate_vnf(host["name"], vnf)

    # Step 2: add the host in the LSP
    print("Adding the host in the LSP")
    pcc = sdn.get_lsp_pcc(lsp)
    sdn.chain_lsp(pcc, lsp, prev_h, host)

def add_lsp(sdn, lsp, path, config):
    """
    Adding a new LSP
    """
    print("Trying to add LSP {} via {}".format(lsp, path))

    # Check the hosts
    ref_hosts = [x for x in config["hosts"] if x["name"] in path]

    hosts_path = []
    for host_name in path:
        for ref_host in ref_hosts:
            if host_name == ref_host["name"]:
                hosts_path.append(ref_host)

    print(hosts_path)

    start_host = hosts_path[0]
    end_host = hosts_path[-1]
    transit_hosts = hosts_path[1:-1]

    sdn.add_lsp(lsp, start_host["ip"], start_host, end_host, transit_hosts)


def deactivate_all():
    pass


def init_sdn(controller, config) -> Type[SDN]:
    """
    Initialize the SDN controller based on the configuration

    @param controller: controller name
    """

    if controller == "opendaylight" or controller == "odl":
        cntrl_conf = config["controllers"]["northstar"]
        odl = OpenDayLight(cntrl_conf["host"], cntrl_conf["user"], cntrl_conf["password"], config)
        return odl
    elif controller == "northstar" or controller == "ns":
        cntrl_conf = config["controllers"]["northstar"]
        ns = NorthStar(cntrl_conf["host"], cntrl_conf["user"], cntrl_conf["password"], config)
        return ns
    else:
        print("Unknown controller {}".format(controller))
        exit(-1)


def list_lsps(sdn, pcc):
    lsps = sdn.get_lsps(pcc)
    print(json.dumps(lsps, indent=2))

def list_vnfs(vnfhosts):
    vnf_lists = get_vnfs(vnfhosts)
    print(json.dumps(vnf_lists, indent=2))

def parse_hosts(config):
    """
    Parses the config and returns the different types of endpoint
    return endpoints, vnfhosts, edgerouters
    """
    hosts = config["hosts"]

    endpoints = []
    vnfhosts = []
    edgerouters = []

    for host in hosts:
        if host["type"] == "endpoint":
            endpoints.append(host)
        elif host["type"] == "vnfhost":
            vnfhosts.append(host)
        elif host["type"] == "edgerouter":
            edgerouters.append(host)
        else:
            raise Exception(f"Unknown host type: {host}")
    
    return endpoints, vnfhosts, edgerouters

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--controller", default="northstar",
        choices=["odl", "ns", "opendaylight", "northstar"])
    subparsers = parser.add_subparsers(dest="action")

    vnf_choices = [x.replace("_FUNC_ID", "") for x in PROG_IDS.keys()]


    parser_list = subparsers.add_parser("list")

    parser_list.add_argument("target", choices=["vnf", "lsp"])
    parser_list.add_argument("--pcc", default=None, required=False, help="List LSPs on PCC")

    # VNF migration
    parser_migrate = subparsers.add_parser("migrate")
    parser_migrate.add_argument("lsp", help="The LSP to migrate")
    parser_migrate.add_argument("host1", help="The host to migrate from")
    parser_migrate.add_argument("host2", help="The host to migrate to")
    parser_migrate.add_argument("vnf", choices = vnf_choices, help="The vnf to migrate")

    # VNF chaining
    parser_chain = subparsers.add_parser("chain")
    parser_chain.add_argument("lsp", help="The LSP to chain in")
    parser_chain.add_argument("host1", help="The host to chain from")
    parser_chain.add_argument("host2", help="The host targetting in the chain")
    parser_chain.add_argument("vnf", choices = vnf_choices, help="The vnf to spin up on the second host")

    # VNF Control
    parser_start = subparsers.add_parser("start")
    parser_start.add_argument("host", help="The host to start the vnf on")
    parser_start.add_argument("vnf", choices=vnf_choices,\
                        help="The vnf to start")

    parser_stop = subparsers.add_parser("stop")
    parser_stop.add_argument("host", help="The host to start the vnf on")
    parser_stop.add_argument("vnf", choices=vnf_choices,\
                        help="The vnf to stop")

    # LSP actions
    parser_lsp = subparsers.add_parser("lsp")
    subparsers_lsp = parser_lsp.add_subparsers(dest="lsp_action", required=True)

    p_lsp_add = subparsers_lsp.add_parser("list")
    p_lsp_add.add_argument("--pcc", default=None, required=False, help="List LSPs on PCC")

    p_lsp_add = subparsers_lsp.add_parser("add") 
    p_lsp_add.add_argument("name")
    p_lsp_add.add_argument("path", nargs="+", help="the path, specified in hosts: vqfx1,host12,vqfx4")

    p_lsp_remove = subparsers_lsp.add_parser("remove")
    p_lsp_remove.add_argument("name", help="The vnf to remove")

    parser_stop.add_argument("host", help="The host to start the vnf on")
    parser_stop.add_argument("vnf", choices=vnf_choices,\
                        help="The vnf to stop")

    args = parser.parse_args()


    with open("./sr-vnf/config.yaml") as f:
        config = yaml.safe_load(f)

    endpoints, vnfhosts, edgerouters = parse_hosts(config)

    sdn = init_sdn(args.controller, config)

    if args.action == "list":
        if args.target == "vnf":
            list_vnfs([x["name"] for x in vnfhosts])
        elif args.target == "lsp":
            list_lsps(sdn, args.pcc)

    if args.action == "migrate":
        lsp = args.lsp
        h1 = args.host1
        h2 = args.host2
        vnf = args.vnf
        migrate(sdn, lsp, h1, h2, vnf, config)

    if args.action == "lsp" and args.lsp_action == "list":
        list_lsps(sdn, args.pcc)
    if args.action == "lsp" and args.lsp_action == "remove":
        name = args.name
        sdn.remove_lsp(name)

    if args.action == "lsp" and args.lsp_action == "add":
        name = args.name

        path = args.path
        
        add_lsp(sdn, name, path, config)

    if args.action == "chain":
        lsp = args.lsp
        prev_h = args.host1
        host = args.host2
        vnf = args.vnf
        chain(sdn, lsp, prev_h, host, vnf, config)

