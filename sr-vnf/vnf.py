from subprocess import run, PIPE
import json

PROG_IDS = {
    "SR_PROXY_FUNC_ID": 0,
    "MPLS_FW_FUNC_ID": 1,
    "MIRROR_FUNC_ID": 4,
    "NFV_SWITCH_FUNC_ID": 7,
    "MPLS_DUMMY_FUNC_ID": 6,
}

"""
Wrapper code for the nfv-py tool
Acts via ssh on remote hosts.
"""

def activate_vnf(host, vnf):
    # Execute the script action on the remote host
    run([
        "ssh",
        "{}".format(host),
        "sudo python3 -m nfv-py --attach {}".format(vnf),
    ])

def deactivate_vnf(host, vnf):
    """
    Deactivates the vnf on the host
    """
    run([
        "ssh",
        "{}".format(host),
        "sudo python3 -m nfv-py --detach {}".format(vnf),
    ])

def get_vnfs(hosts):
    """
    Get all the VNFs from the given hosts
    """

    vnf_lists = {}
    for host in hosts:
        # print("Host {}".format(host))
        res = run([
            "ssh",
            "{}".format(host),
            "sudo python3 -m nfv-py --list --json",
        ], stdout=PIPE)

        try:
            vnf_list = json.loads(res.stdout)
            vnf_lists[host] = vnf_list
        except json.decoder.JSONDecodeError:
            raise Exception(f"Can't get VNFs:\n{res.stdout}\n{res.stderr}")

    return vnf_lists