from typing import List, Dict, Type, Tuple
import requests
import json

from .sdn import SDN

class OpenDayLight(SDN):
    """
    Specific SDN implementation for the Juniper NorthStar SDN controller
    """
    def __init__(self, host, user, passwd, config):
        self.host = "http://" + host
        self.user = user
        self.passwd = passwd
        self.config = config


    def add_lsp(self, name, pcc, source, destination, route: List[Dict]):

        eros = []
        for obj in route + [destination]:
            ero = {
                "loose": "true",
                "odl-pcep-segment-routing:m-flag": "true",
                "odl-pcep-segment-routing:sid-type": "ipv4-node-id",
                "odl-pcep-segment-routing:sid": "{}".format(obj["sid"]),
                "odl-pcep-segment-routing:ip-address": "{}".format(obj["ip"])
            }
            eros.append(ero)

        data = \
            {
                "input": {
                    "network-topology-ref": "/network-topology:network-topology/network-topology:topology[network-topology:topology-id=\"pcep-topology\"]",
                    "arguments": {
                        "metadata": {},
                        "endpoints-obj": {
                            "ipv4": {
                            "source-ipv4-address": "{}".format(source["ip"]),
                            "destination-ipv4-address": "{}".format(destination["ip"]),
                            }
                        },
                        "ero": {
                            "subobject": eros
                        },
                        "odl-pcep-ietf-stateful07:lsp": {
                            "delegate": "true",
                            "administrative": "true"
                        },
                        "odl-pcep-ietf-stateful07:path-setup-type": {
                            "pst": "1"
                        }
                    },
                    "node": "pcc://{}".format(pcc),
                    "name": "{}".format(name)
                }
            }

        uri = self.host + "/restconf/operations/network-topology-pcep:add-lsp"
        r = requests.post(uri, auth=(self.user, self.passwd),
            verify=False, json=data)

        print(r)

    def remove_lsp(self, name, pcc):
        """
        /restconf/operations/network-topology-pcep:remove-lsp
        """

        data = \
        {
        "input": {
                "network-topology-ref": "/network-topology:network-topology/network-topology:topology[network-topology:topology-id=\"pcep-topology\"]",
                "node": "pcc://{}".format(pcc),
                "name": "{}".format(name)
            }
        }

        uri =self.host + "/restconf/operations/network-topology-pcep:remove-lsp"

        r = requests.post(uri, auth=(self.user, self.passwd),
            verify=False, json=data)

        print(r)

    def chain_lsp(self, pcc, name, prev_h, h):
        """
        Add host h after host prev_h in the given LSP
        """
        # Get the LSPs first

        lsps = self.get_lsps(pcc=pcc)
        # print(lsps)

        
        lsp = [x for x in lsps if x["name"] == name]
        if len(lsp) == 0:
            raise Exception("No such LSP")
        else:
            lsp = lsp[0]
        # print(lsp)

        # Isolate the eros, update it
        eros = lsp["path"][0]["ero"]["subobject"]
        print("Old path")
        print(json.dumps(eros, indent=2))

        new_ero = {
                "loose": "true",
                "odl-pcep-segment-routing:m-flag": "true",
                "odl-pcep-segment-routing:sid-type": "ipv4-node-id",
                "odl-pcep-segment-routing:sid": "{}".format(h["sid"]),
                "odl-pcep-segment-routing:ip-address": "{}".format(h["ip"])
            }

        # Build a list of new eros
        new_path = []

        for ero in eros:
            old_ero = {
                "loose": "true",
                "odl-pcep-segment-routing:m-flag": "true",
                "odl-pcep-segment-routing:sid-type": "ipv4-node-id",
                "odl-pcep-segment-routing:sid": "{}".\
                    format(ero["odl-pcep-segment-routing:sid"]),
                "odl-pcep-segment-routing:ip-address": "{}".\
                    format(ero["odl-pcep-segment-routing:ip-address"])
            }
            new_path.append(old_ero)
            if ero["odl-pcep-segment-routing:ip-address"] == prev_h["ip"]:
                # The insert the ero + the new ero
                new_path.append(new_ero)
            
        print("New path")
        print(json.dumps(new_path, indent=2))

        self.update_lsp(name, pcc, new_path)


    def get_lsps(self, pcc=None):
        """
        Get all lsps, or lsps on a specific pcc if given
        """
        uri = self.host + "/restconf/operational/network-topology:network-topology/topology/pcep-topology"

        r = requests.get(uri, auth=(self.user, self.passwd), verify=False)

        pcep_topo = json.loads(r.content)
        pcep_topo = pcep_topo["topology"][0]

        nodes = pcep_topo["node"]
        lsps = []
        for node in nodes:
            # print("Node {}".format(node["node-id"]))
            # print(node["network-topology-pcep:path-computation-client"])
            if "reported-lsp" in node["network-topology-pcep:path-computation-client"]:
                for lsp in node["network-topology-pcep:path-computation-client"]["reported-lsp"]:
                    # print("\t{}".format(lsp["name"]))
                    # print("\t{}".format(lsp["path"][0]["odl-pcep-ietf-stateful07:lsp"]["tlvs"]["lsp-identifiers"]["ipv4"]))

                    if pcc != None and node["node-id"] == "pcc://{}".format(pcc):
                        lsps.append(lsp)
                    elif pcc == None:
                        lsps.append(lsp)

        return lsps

    def update_lsp(self, name, pcc, eros):
        print("Updating lsp")
        data = \
            {"input": 
                {
                "network-topology-ref": "/network-topology:network-topology/network-topology:topology[network-topology:topology-id=\"pcep-topology\"]",
                "arguments": {
                    "ero": {
                        "subobject": eros
                    },
                    "odl-pcep-ietf-stateful07:lsp": {
                        "delegate": "true",
                        "administrative": "true"
                    },
                    "odl-pcep-ietf-stateful07:path-setup-type": {
                        "pst": "1"
                    }
                },
                "node": "pcc://{}".format(pcc),
                "name": "{}".format(name)
                }
            }

        # print(json.dumps(data, indent=2))

        uri = self.host + "/restconf/operations/network-topology-pcep:update-lsp"

        r = requests.post(uri, auth=(self.user, self.passwd),
            verify=False, json=data)
        print(r)
        print(r.content)

    def migrate_lsp(self, name, h1, h2):

        # Get the LSP from ODL
        lsps = self.get_lsps()
        
        lsp = [x for x in lsps if x["name"] == name][0]

        # print("Got the lsp:\n{}".format(lsp))
        # print("Changing {} to {}".format(h1, h2))

        # First figure out what to change sid/ip wise

        hosts = self.config["hosts"]
        host1 = [x for x in hosts if x["name"] == h1][0]
        host2 = [x for x in hosts if x["name"] == h2][0]
        # print(host1)
        # print(host2)
        path = lsp["path"][0]
        # print(path)
        # print(path["ero"]["subobject"])
        new_eros = []
        for srero in path["ero"]["subobject"]:
            # print(srero)
            if srero["odl-pcep-segment-routing:ip-address"] == host1["ip"]:
                print("Replacing")
                srero["odl-pcep-segment-routing:ip-address"] = host2["ip"]
                srero["odl-pcep-segment-routing:sid"] = host2["sid"]

            ero = {
                "loose": "true",
                "odl-pcep-segment-routing:m-flag": "true",
                "odl-pcep-segment-routing:sid": "{}".format(srero["odl-pcep-segment-routing:sid"]),
                "odl-pcep-segment-routing:sid-type": "ipv4-node-id",
                "odl-pcep-segment-routing:ip-address": "{}".format( srero["odl-pcep-segment-routing:ip-address"])
            }

            new_eros.append(ero)


        # print(new_eros)

        # print(lsp)

        self.update_lsp(name, "10.0.0.1", new_eros)
