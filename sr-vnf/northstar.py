from typing import List, Dict
import requests
import json

from .sdn import SDN

api_url = "/NorthStar/API/v2/tenant/1/"

class NorthStar(SDN):
    """
    Specific SDN implementation for the Juniper NorthStar SDN controller
    """
    def __init__(self, host, user, passwd, config):
        
        self.host = "https://" + host
        self.user = user
        self.passwd = passwd

        # get the header used for every request
        # with the token inserted
        self.auth_header = self._get_token()

        self.config = config

    def _get_token(self):
        """
        Acquires the authentication token
        @return: the authentication headers with the token needed for the subsequent requests
        """

        data = '{{"grant_type":"password","username":"{}","password":"{}"}}'\
                    .format(self.user, self.passwd)

        r = requests.post(self.host + "/oauth2/token" , auth=(self.user, self.passwd),
        verify=False, data=json.loads(data))

        resp = json.loads(r.content)
        headers = {
            "Authorization": "{} {}".format(resp["token_type"], resp["access_token"])
        }

        return headers

    def get_lsps(self, pcc=None):
        r = requests.get(self.host + api_url + "topology/1/te-lsps",
            headers = self.auth_header, verify=False)

        lsps = json.loads(r.content)

        # Filter, if needed
        if pcc != None:
            lsps = [x for x in lsps if x["from"]["address"] == pcc]

        return lsps


    def update_lsp(self, name, source_ip, destination_ip, eros):

        print("Updating LSP")

        # First get the index
        lsps = self.get_lsps()
        lsp = [x for x in lsps if x["name"] == name][0]
        lsp_index = self._get_lsp_index(name)

        data = \
        {
            "lspIndex": lsp_index,
            "name": "{}".format(name),
            "from": {"topoObjectType": "ipv4","address": "{}".format(source_ip)},
            "to":   {"topoObjectType": "ipv4","address": "{}".format(destination_ip)},
            "provisioningType": "SR",
            "pathType": "primary",
            "plannedProperties": {
                "design": {
                    "routingMethod": "routeByDevice"
                },
                "bandwidth": 0,
                "setupPriority": 7,
                "holdingPriority": 7,
                "ero": eros
            }
        }


        uri = self.host + api_url + "topology/1/te-lsps/{}".format(lsp_index)

        r = requests.put(uri, headers = self.auth_header, verify=False, json=data)
        print(r)
        print(r.content)

    def migrate_lsp(self, name, h1, h2):

        hosts = self.config["hosts"]
        host1 = [x for x in hosts if x["name"] == h1][0]
        host2 = [x for x in hosts if x["name"] == h2][0]

        lsps = self.get_lsps()
        lsp = [x for x in lsps if x["name"] == name][0]

        lsp_index = self._get_lsp_index(name)

        print(lsp)
        print(lsp["plannedProperties"])
        old_eros = lsp["plannedProperties"]["ero"]
        new_eros = []
        for old_ero in old_eros:
            if old_ero["address"] == host1["ip"]:
                new_ero = {
                    "topoObjectType": "ipv4",
                    "address": "{}".format(host2["ip"]),
                    "loose": True
                }
                new_eros.append(new_ero)
            else:
                new_eros.append(old_ero)

        print("Migrated path")
        print(json.dumps(new_eros, indent=2))

        source_ip = lsp["from"]["address"]
        destination_ip = lsp["to"]["address"]

        self.update_lsp(name, source_ip, destination_ip, new_eros)

    def chain_lsp(self, pcc, name, prev_h, h):
        """
        Add host h after host prev_h in the given LSP
        """

        lsps = self.get_lsps()
        lsp = [x for x in lsps if x["name"] == name][0]
        eros = []


        new_ero = {
                    "topoObjectType": "ipv4",
                    "address": "{}".format(h["ip"]),
                    "loose": True
                }

        old_eros = lsp["plannedProperties"]["ero"]
        for ero in old_eros:
            eros.append(ero)
            if ero["address"] == prev_h["ip"]:
                eros.append(new_ero)

        print("New path")
        print(json.dumps(eros, indent=2))

        source_ip = lsp["from"]["address"]
        destination_ip = lsp["to"]["address"]
        self.update_lsp(name, source_ip, destination_ip, eros)

    def add_lsp(self, name, pcc, source, destination, route: List[Dict] ):
        """
        Add an SR PCEP LSP

        Routed by device to be able to use the loop backs of the transit routers
        and to use their node sids
        """

        eros = []
        # Add the starting node
        startero = {
            "topoObjectType": "ipv4",
            "address": "{}".format(source["ip"]),
            "loose": False
        }
        eros.append(startero)
        for node in route + [destination]:
            print(node)
            ero = {
                "topoObjectType": "ipv4",
                "address": "{}".format(node["ip"]),
                "loose": True
            }
            eros.append(ero)

        data = \
        {
            "name": "{}".format(name),
            "from": {"topoObjectType": "ipv4","address": "{}".format(source["ip"])},
            "to":   {"topoObjectType": "ipv4","address": "{}".format(destination["ip"])},
            "provisioningType": "SR",
            "plannedProperties": {
                "design": {
                    "routingMethod": "routeByDevice"
                },
                "bandwidth": 0,
                "setupPriority": 7,
                "holdingPriority": 7,
                "ero": eros
            }
        }

        uri = self.host + api_url + "topology/1/te-lsps"

        r = requests.post(uri,
            headers = self.auth_header, verify=False, json=data)

        print(r)
        path_data = json.loads(r.content)
        print(json.dumps(path_data, indent=2))


    def _get_lsp_index(self, name, pcc=None):
        """
        Get LSP index, matching on name and if needed on pcc
        """
        lsps = self.get_lsps()
        
        # filter by name
        lsp = [x for x in lsps if x["name"] == name]
        # filter by pcc if needed
        if pcc != None:
            lsp = [x for x in lsps if x["from"]["address"] == pcc]

        if len(lsp) == 0:
            raise Exception("No such LSP")
        else:
            lsp = lsp[0]

        lsp_index = lsp["lspIndex"]

        return lsp_index
    
    def get_lsp_pcc(self, name):
        """
        Get the PCC for a specific lsp
        """

        lsps = self.get_lsps()
        lsp = [x for x in lsps if x["name"] == name][0]

        return lsp["from"]["address"]

    def remove_lsp(self, name, pcc=None):
        # First get the correct lsp
        lsp_index = self._get_lsp_index(name, pcc)
        uri = self.host + api_url + "topology/1/te-lsps/{}".format(lsp_index)

        r = requests.delete(uri, headers = self.auth_header, verify=False)
        print(r)