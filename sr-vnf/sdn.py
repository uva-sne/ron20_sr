from typing import List, Dict, Type, Tuple
class SDN:
    def add_lsp(self, name, pcc, source, destination, route: List[Dict]):
        """
        Adds a LSP with the given name and route

        @param name: name of the sr-lsp
        @param pcc: identifier for the pcc that needs to receive the sr-lsp
        @param source: start of the sr-lsp
        @param destination: end of the sr-lsp
        @param route: the route to be followed for the SR-LSP
        """
        raise NotImplementedError()

    def remove_lsp(self, name, pcc=None):
        """
        Remove sr-lsp with name, on pcc if given
        """
        raise NotImplementedError()

    def get_lsps(self, pcc=None):
        """
        List all lsps, on pcc if given
        """
        raise NotImplementedError()

    def get_lsp_pcc(self, name):
        """
        Get the PCC for a specific sr-lsp
        """
        raise NotImplementedError()
